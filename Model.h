#define __MODEL_H__

#include <vector>
#include "geometry.h"
#include "tgaimage.h"

class Model
{
public:
    //std::vector<std::vector<float> > verts;
    std::vector<Vec3f> verts;
    std::vector<std::vector<Vec3i> > faces;
    std::vector<Vec3f> vertsn;
    std::vector<Vec2f> vt_;
    Model(const char *filename);
    int nverts();
    int nfaces();
    Vec3f vert(int i);
    Vec3f vertn(int i);
    Vec3f norm(int iface, int nvert);
    std::vector<int> face(int i);
    TGAImage diffusemap;
    TGAColor diffuse(Vec2i uv);
    Vec2i vt(int iface, int nvert);
    void load_texture(std::string filename, const char *suffix, TGAImage &img);
};
