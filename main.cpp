#include "tgaimage.h"
#include <cmath>
#include <fstream>
#include "Model.h"
#include <iostream>
#include <string>
#include <sstream>
#include <stdlib.h>
#include <limits>
#include "geometry.h"

const int width = 1024;
const int height = 1024;
const int depth = 1000;
Vec3f eye(1,1,3);
Vec3f center(0,0,0);
Vec3f light_dir = Vec3f(0,1,1).normalize();

int inf = std::numeric_limits<int>::min();
float zbuffer[width][height];

Model *model = NULL;


std::vector<std::vector<int> > line_alt(TGAImage &image, int x0, int y0, int z0 , int x1, int y1, int z1, int color1, int color2, bool trace, std::vector<Vec2i> triangle,Vec2i vt0, Vec2i vt1,Vec2i vt2)
{
    std::vector<std::vector<int> > points;

    int dx = abs(x1-x0);
    int dy = abs(y1-y0);

    int distance = std::max(dx,dy) + 1;
    int diffColor = std::abs(color1 - color2);
    float pas = (color1>color2?-((float)diffColor / (float)distance):((float)diffColor / (float)distance));
    float colorAlt = color1;

    int diffZ = std::abs(z0 - z1);
    float pasZ = (z0>z1?-((float)diffZ / (float)distance):((float)diffZ / (float)distance));
    float z = z0;

    int sx = (x0<x1?1:-1);
    int sy = (y0<y1?1:-1);

    int err = dx-dy;
    int lastx=-1;

    while(!(x0==x1 && y0==y1))
    {
        if(x0 == lastx)
        {
            if(points.size()>0)
            {
                points.pop_back();
            }
        }

        std::vector<int> point;
        point.push_back(x0);
        point.push_back(y0);
        point.push_back(z);
        point.push_back(round(colorAlt));
        points.push_back(point);

        colorAlt += pas;
        z += pasZ;

        lastx = x0;

        if(colorAlt < 0)
        {
            colorAlt = 0;
        }
        else if(colorAlt > 255)
        {
            colorAlt = 255;
        }

        if(x0 >=0 && y0 >=0)
        {
            if(trace == true && z > zbuffer[x0][y0])
            {

                float a = ((triangle[0][0]-triangle[2][0])*(triangle[1][1]-triangle[2][1]) - (triangle[0][1]-triangle[2][1])*(triangle[1][0]-triangle[2][0]));

                if(a != 0)
                {
                    float b = (x0-triangle[2][0])*(triangle[1][1]-triangle[2][1]) - (y0-triangle[2][1])*(triangle[1][0]-triangle[2][0]);
                    float c = (x0-triangle[2][0])*(triangle[2][1]-triangle[0][1]) - (y0-triangle[2][1])*(triangle[2][0]-triangle[0][0]);
                    float d = (x0-triangle[1][0])*(triangle[0][1]-triangle[1][1]) - (y0-triangle[1][1])*(triangle[0][0]-triangle[1][0]);

                    float u = b/a;
                    float v = c/a;
                    float w = d/a;

                    int xt = u*vt0[0] + v*vt1[0] + w*vt2[0];
                    int yt = u*vt0[1] + v*vt1[1] + w*vt2[1];
                    Vec2i vtt;
                    vtt[0] = xt;
                    vtt[1] = yt;

                    TGAColor col = model->diffuse(vtt);
                    float intensity = colorAlt / 255;
                    image.set(x0,y0, TGAColor( col.r*intensity, col.g*intensity, col.b*intensity, 1));
                    //image.set(x0, y0, TGAColor(round(colorAlt) ,round(colorAlt),round(colorAlt),1));
                    zbuffer[x0][y0] = z;
                }



            }
        }


        int e2 = 2*err;

        if(e2 > -dy)
        {
            err -= dy;
            x0 += sx;
        }

        if(e2 < dx)
        {
            err += dx;
            y0 += sy;
        }

    }


    if(x1 == lastx)
    {
        if(points.size()>0)
        {
            points.pop_back();
        }
    }

    std::vector<int> point;
    point.push_back(x1);
    point.push_back(y1);
    point.push_back(z1);
    point.push_back(color2);
    points.push_back(point);

    if(x1 >=0 && y1 >=0)
    {
        if(trace == true && z1 > zbuffer[x1][y1])
        {
            float a = ((triangle[0][0]-triangle[2][0])*(triangle[1][1]-triangle[2][1]) - (triangle[0][1]-triangle[2][1])*(triangle[1][0]-triangle[2][0]));

            if(a != 0)
            {
                float b = (x1-triangle[2][0])*(triangle[1][1]-triangle[2][1]) - (y1-triangle[2][1])*(triangle[1][0]-triangle[2][0]);
                float c = (x1-triangle[2][0])*(triangle[2][1]-triangle[0][1]) - (y1-triangle[2][1])*(triangle[2][0]-triangle[0][0]);
                float d = (x1-triangle[1][0])*(triangle[0][1]-triangle[1][1]) - (y1-triangle[1][1])*(triangle[0][0]-triangle[1][0]);

                float u = b/a;
                float v = c/a;
                float w = d/a;

                //std::cout << "***********\n" << u << " " << v << " " << w << "\n";
                int xt = u*vt0[0] + v*vt1[0] + w*vt2[0];
                int yt = u*vt0[1] + v*vt1[1] + w*vt2[1];
                //std::cout << xt << " " << yt << "\n";
                Vec2i vtt;
                vtt[0] = xt;
                vtt[1] = yt;

                    TGAColor col = model->diffuse(vtt);
                    float intensity = colorAlt / 255;
                    image.set(x1,y1, TGAColor( col.r*intensity, col.g*intensity, col.b*intensity, 1));
                //image.set(x0, y0, TGAColor(round(colorAlt) ,round(colorAlt),round(colorAlt),1));
                zbuffer[x1][y1] = z1;
            }
            /*image.set(x1, y1, TGAColor(color2,color2,color2,1));
            zbuffer[x1][y1] = z1;*/
        }
    }

    //image.set(xa,ya, model->diffuse(vt0));
    //image.set(x1,y1, model->diffuse(vt1));

    return (points);
}

void triangle_alt(TGAImage &image, int x0, int y0, int z0, int x1, int y1, int z1, int x2, int y2, int z2, float col0, float col1, float col2, Vec2i vt0, Vec2i vt1,Vec2i vt2)
{
    std::vector<Vec2i> triangle;
    Vec2i p1;
    p1[0] = x0;
    p1[1] = y0;

    Vec2i p2;
    p2[0] = x1;
    p2[1] = y1;

    Vec2i p3;
    p3[0] = x2;
    p3[1] = y2;

    triangle.push_back(p1);
    triangle.push_back(p2);
    triangle.push_back(p3);

    col0 *= 255;
    col1 *= 255;
    col2 *= 255;

    if(col0 < 0)
    {
        col0 = 0;
    }

    else if(col0 > 255)
    {
        col0 = 255;
    }

    if(col1 < 0)
    {
        col1 = 0;
    }
    else if(col1 > 255)
    {
        col1 = 255;
    }

    if(col2 < 0)
    {
        col2 = 0;
    }
    else if(col2 > 255)
    {
        col2 = 255;
    }

    if (x0 > x1)
    {
        std::swap(x0, x1);
        std::swap(y0, y1);
        std::swap(z0,z1);
        //std::swap(vt0,vt1);
        std::swap(col0,col1);
    }

    if (x0 > x2)
    {
        std::swap(x0, x2);
        std::swap(y0, y2);
        std::swap(z0,z2);
        //std::swap(vt0,vt2);
        std::swap(col0,col2);
    }

    if (x1 > x2)
    {
        std::swap(x1, x2);
        std::swap(y1, y2);
        std::swap(z1,z2);
        //std::swap(vt1,vt2);
        std::swap(col1,col2);
    }

    std::vector<std::vector<int> > line01 = line_alt(image, x0, y0, z0, x1, y1 , z1, col0,col1,true, triangle, vt0,vt1,vt2);
    std::vector<std::vector<int> > line02 = line_alt(image, x0, y0, z0, x2, y2 , z2, col0,col2,true, triangle, vt0,vt1,vt2);
    std::vector<std::vector<int> > line12 = line_alt(image, x1, y1, z1, x2, y2 , z2, col1,col2,true, triangle, vt0,vt1,vt2);

    int x;

    for(x = 0; x < line01.size(); x++)
    {
        std::vector<int> point1 = line01[x];
        std::vector<int> point2 = line02[x];
        line_alt(image, point1[0],point1[1],point1[2],point2[0],point2[1],point2[2], point1[3],point2[3],true,triangle, vt0,vt1,vt2);
    }

    for(x = 1; x < line12.size(); x++)
    {
        std::vector<int> point1 = line12[x];
        std::vector<int> point2 = line02[line01.size() + x -1];
        line_alt(image, point1[0],point1[1],point1[2],point2[0],point2[1],point2[2], point1[3],point2[3],true,triangle, vt0,vt1,vt2);

    }
}

Matrix viewport(int x, int y, int w, int h)
{
    Matrix m = Matrix::identity(4);
    m[0][3] = x+w/2.f;
    m[1][3] = y+h/2.f;
    m[2][3] = depth/2.f;
    m[0][0] = w/2.f;
    m[1][1] = h/2.f;
    m[2][2] = depth/2.f;
    return m;
}
Matrix lookat(Vec3f eye, Vec3f center, Vec3f up)
{
    Vec3f z = (eye-center).normalize();
    Vec3f x = (up^z).normalize();
    Vec3f y = (z^x).normalize();
    Matrix res = Matrix::identity(4);
    for (int i=0; i<3; i++)
    {
        res[0][i] = x[i];
        res[1][i] = y[i];
        res[2][i] = z[i];
        res[3][i] = center[i];
    }
    return res;
}

int main()
{
    int i;
    int j;

    for(i = 0; i < width; i++)
    {
        for(j = 0; j < height; j++)
        {
            zbuffer[i][j] = inf;
        }
    }
    model = new Model("./african_head.obj");

    TGAImage image(width, height, TGAImage::RGB);

    printf("%i\n", model->nfaces());

    Matrix ModelView = lookat(eye, center, Vec3f(0,1,0));
    Matrix Projection = Matrix::identity(4);
    Matrix ViewPort = viewport(width/8, height/8, width*3/4, height*3/4);
    Projection[3][2] = -1.f/(eye-center).norm();

    Matrix z = (ViewPort*Projection*ModelView);

    for(int i = 0; i < model->nfaces(); i++)
    {
        std::vector<int> face = model->face(i);

        Vec3i screen_coords[3];
        Vec3f world_coords[3];
        float intensity[3];

        for (int j=0; j<3; j++)
        {
            Vec3f v = model->vert(face[j]);
            screen_coords[j] = Vec3f(ViewPort*Projection*ModelView*Matrix(v));
            world_coords[j] = v;
            intensity[j] = model->norm(i, j)*light_dir;
        }

        Vec2i vt[3];
        for (int k=0; k<3; k++)
        {
            vt[k] = model->vt(i, k);
        }

        triangle_alt(image,screen_coords[0][0],screen_coords[0][1],screen_coords[0][2],screen_coords[1][0],screen_coords[1][1],screen_coords[1][2],screen_coords[2][0],screen_coords[2][1],screen_coords[2][2],intensity[0], intensity[1], intensity[2], vt[0],vt[1],vt[2]);

    }

    image.flip_vertically();
    image.write_tga_file("dump.tga");

    TGAImage buff(width, height, TGAImage::RGB);

    int a,b;
    for(a = 0; a < width; a++)
    {
        for(b = 0; b < height; b++)
        {
            float color = zbuffer[a][b]/(float)depth*255;
            if(color < 0)
            {
                color = 0;
            }
            else if(color > 255)
            {
                color = 255;
            }
            buff.set(a, b, TGAColor(round(color),round(color),round(color),1));
        }
    }

    buff.flip_vertically();
    buff.write_tga_file("zbuffer.tga");

    return 0;
}
