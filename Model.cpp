#include <iostream>
#include <fstream>
#include <string>
#include "Model.h"
#include <sstream>
#include <stdlib.h>
#include "geometry.h";

using namespace std;


Model::Model(const char *filename)
{
    string line;
    ifstream myfile (filename);
    if (myfile.is_open())
    {
        while (getline(myfile,line) )
        {
            std::istringstream iss(line.c_str());

            char name;

            if(!line.compare(0,2,"v "))
            {
                //std::vector<float> v;
                Vec3f v;
                iss >> name;

                float x,y,z;

                iss >> x >> y >> z;

                v[0] = x;
                v[1] = y;
                v[2] = z;
                /*v.push_back(x);
                v.push_back(y);
                v.push_back(z);*/

                verts.push_back(v);
            }
            else if(!line.compare(0,2,"f "))
            {
                /*std::vector<int> f;
                char trash;
                int itrash, idx;
                iss >> trash;

                while (iss >> idx >> trash >> itrash >> trash >> itrash)
                {
                    idx--; // in wavefront obj all indices start at 1, not zero
                    f.push_back(idx);
                }

                faces.push_back(f);*/
                std::vector<Vec3i> f;
                Vec3i tmp;
                iss >> name;
                while (iss >> tmp[0] >> name >> tmp[1] >> name >> tmp[2])
                {
                    for (int i=0; i<3; i++) tmp[i]--; // in wavefront obj all indices start at 1, not zero
                    f.push_back(tmp);
                }
                faces.push_back(f);
            }
            else if(!line.compare(0,2,"vn"))
            {
                //std::vector<float> vn;

                Vec3f vn;
                iss >> name;
                iss >> name;

                float x,y,z;


                iss >> x >> y >> z;

                vn[0] = x;
                vn[1] = y;
                vn[2] = z;
                /*vn.push_back(x);
                vn.push_back(y);
                vn.push_back(z);*/

                vertsn.push_back(vn);
            }
            else if (!line.compare(0, 3, "vt "))
            {
                iss >> name >> name;
                Vec2f v;
                for (int i=0; i<2; i++) iss >> v[i];
                vt_.push_back(v);
            }
        }
        load_texture(filename, "_diffuse.tga", diffusemap);
        myfile.close();
    }

    else cout << "Unable to open file";
}

int Model::nverts()
{
    return (int)verts.size();
}

int Model::nfaces()
{
    return (int)faces.size();
}

std::vector<int> Model::face(int idx)
{
    std::vector<int> face;
    for (int i=0; i<(int)faces[idx].size(); i++) face.push_back(faces[idx][i][0]);
    return face;
}

Vec3f Model::vert(int i)
{
    return verts[i];
}

Vec3f Model::vertn(int i)
{
    return vertsn[i];
}

Vec3f Model::norm(int iface, int nvert)
{
    int idx = faces[iface][nvert][2];
    return vertsn[idx].normalize();
}

void Model::load_texture(std::string filename, const char *suffix, TGAImage &img)
{
    std::string texfile(filename);
    size_t dot = texfile.find_last_of(".");
    if (dot!=std::string::npos)
    {
        texfile = texfile.substr(0,dot) + std::string(suffix);
        std::cerr << "texture file " << texfile << " loading " << (img.read_tga_file(texfile.c_str()) ? "ok" : "failed") << std::endl;
        img.flip_vertically();
    }
}

TGAColor Model::diffuse(Vec2i vt)
{
    return diffusemap.get(vt.x, vt.y);
}

Vec2i Model::vt(int iface, int nvert)
{
    int idx = faces[iface][nvert][1];
    return Vec2i(vt_[idx].x*diffusemap.get_width(), vt_[idx].y*diffusemap.get_height());
}
